A simple module for Polybar to handle basic operations of Spotify music player from the bar itself.

The original author/project of spotify.sh bash script is : https://github.com/NicholasFeldman/dotfiles/blob/master/polybar/.config/polybar/spotify.sh

Operations enabled are :

    Right-click -> Play/Pause

    Scroll-up -> Next track

    Scroll-down -> Previous track

Feel free to update/repost the module.